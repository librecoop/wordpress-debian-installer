echo Input wordpress database name:
read db_name
echo Input wordpress database user:
read db_user
echo Input worpdress database user password:
read db_pass

#Install all necesary dependencies
apt install curl -y
apt install nginx -y
apt install mariadb-server -y
apt install ufw -y
apt install php-fpm php-curl php-gd php-intl php-mbstring php-soap php-xml php-xmlrpc php-zip php-mysql -y
systemctl start nginx && systemctl enable nginx

#Set up the firewall
ufw allow 80/tcp
ufw allow 443/tcp
ufw allow ssh
ufw enable

#Set up database

cp ./wordpress_db_setup.sql ./wordpress_db_setup_auth.sql
sed -i -e "s/@dbname/$db_name/g" ./wordpress_db_setup_auth.sql
sed -i -e "s/@dbuser/'$db_user'/g" ./wordpress_db_setup_auth.sql
sed -i -e "s/@dbpass/'$db_pass'/g" ./wordpress_db_setup_auth.sql
mariadb -u root < ./wordpress_db_setup_auth.sql
rm ./wordpress_db_setup_auth.sql
sed 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/' /etc/php/7.4/fpm/php.ini
systemctl restart php7.4-fpm

#Update nginx configuration
cp ./default_nginx_availabe_sites /etc/nginx/sites-available/default
nginx -t
systemctl restart nginx

#Downlad Wordpress
cd /tmp
curl -LO https://wordpress.org/latest.tar.gz
tar xzvf latest.tar.gz
cp -a /tmp/wordpress/. /var/www/html/wordpress
cd /var/www/html/wordpress
cp wp-config-sample.php wp-config.php
chown -R www-data:www-data /var/www/html/wordpress/
#Setup database config values
sed -i -e "s/'database_name_here'/'$db_name'/" /var/www/html/wordpress/wp-config.php
sed -i -e "s/'username_here'/'$db_user'/" /var/www/html/wordpress/wp-config.php
sed -i -e "s/'password_here'/'$db_pass'/" /var/www/html/wordpress/wp-config.php


#Set up salt config values
sed -i "/define( 'AUTH_KEY',         'put your unique phrase here' );/d" /var/www/html/wordpress/wp-config.php
sed -i "/define( 'SECURE_AUTH_KEY',  'put your unique phrase here' );/d" /var/www/html/wordpress/wp-config.php
sed -i "/define( 'LOGGED_IN_KEY',    'put your unique phrase here' );/d" /var/www/html/wordpress/wp-config.php
sed -i "/define( 'NONCE_KEY',        'put your unique phrase here' );/d" /var/www/html/wordpress/wp-config.php
sed -i "/define( 'AUTH_SALT',        'put your unique phrase here' );/d" /var/www/html/wordpress/wp-config.php
sed -i "/define( 'SECURE_AUTH_SALT', 'put your unique phrase here' );/d" /var/www/html/wordpress/wp-config.php
sed -i "/define( 'LOGGED_IN_SALT',   'put your unique phrase here' );/d" /var/www/html/wordpress/wp-config.php
sed -i "/define( 'NONCE_SALT',       'put your unique phrase here' );/d" /var/www/html/wordpress/wp-config.php
curl -s https://api.wordpress.org/secret-key/1.1/salt/ >>  /var/www/html/wordpress/wp-config.php

