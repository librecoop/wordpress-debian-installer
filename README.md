# Bash script to install and configure wordpress on a brand new Debian 11 install

This script installs php, mariadb, nginx and wordpress and creates a database for the installation to use. The script assumes a brand new installation and will override the default site configuration for nginx.

The script was made following this two tutorials:
- [Install WordPress with Nginx on Ubuntu 20.04](https://www.hostnextra.com/kb/install-wordpress-with-nginx-on-ubuntu/)
- [How To Install WordPress with LEMP (Nginx, MariaDB and PHP) on Debian 10](https://www.digitalocean.com/community/tutorials/how-to-install-wordpress-with-lemp-nginx-mariadb-and-php-on-debian-10)

Once the script is finished you must access http://(your-ip-address)/wordpress to finish the installation.