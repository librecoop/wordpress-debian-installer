CREATE DATABASE @dbname DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
GRANT ALL ON @dbname.* TO @dbuser@'localhost' IDENTIFIED BY @dbpass;
FLUSH PRIVILEGES;